public class Kingpin extends AbstractGamePiece
{
    public Kingpin()
    {
        super("Kingpin", "K", PLAYER_OUTLAWS);
    }
    public boolean hasEscaped()
    {
        return true;
    }
    public boolean isCaptured(GameBoard gameBoard)
    {
        return true;
    }
}

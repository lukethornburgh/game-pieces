public class Deputy extends AbstractGamePiece
{
    public Deputy()
    {
        super("Deputy", "D", PLAYER_POSSE);
    }
    public boolean hasEscaped()
    {
        return false;
    }
}

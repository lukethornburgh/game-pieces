import java.util.*;
abstract public class AbstractGamePiece
{
    static public final int PLAYER_OUTLAWS = 0;
    static public final int PLAYER_POSSE = 1;
    protected int myCol;
    protected int myRow;
    protected int myPlayerType;
    private String myAbbreviation;
    private String myName;
    abstract public boolean hasEscaped();
    public AbstractGamePiece(String name, String abbreviation, int playerType)
    {
        myName = name;
        myAbbreviation = abbreviation;
        myPlayerType = playerType;
    }
    public int getPlayerType()
    {
        return myPlayerType;
    }
    public int getRow()
    {
        return myRow;
    }
    public int getCol()
    {
        return myCol;
    }
    public String toString()
    {
        if (myPlayerType == PLAYER_OUTLAWS)
        {
            return "Outlaw " + myName + " at column " + myCol + " and row " + myRow;
        }
        else
        {
            return "Posse " + myName + " at column " + myCol + " and row " + myRow;
        }
    }
    public void setPosition(int col, int row)
    {
        myCol = col;
        myRow = row;
    }
    public String getAbbreviation()
    {
        return myAbbreviation;
    }
    public boolean canMoveToLocation(List<GameSquare> path)
    {
        return false;
    }
    public boolean isCaptured(GameBoard gameBoard)
    {
        return false;
    }
}

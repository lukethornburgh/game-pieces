import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

public class GamePieces extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        primaryStage.setTitle("Game Piece Verification");
        primaryStage.setWidth(800);
        primaryStage.setHeight(150);

        GridPane root = new GridPane();
        root.setPadding(new Insets(10));
        primaryStage.setScene(new Scene(root));

        int numColumns = 5;

        for(int c = 0; c < numColumns; c++)
        {
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(100 / (double) numColumns);
            root.getColumnConstraints().add(column);
        }

        AbstractGamePiece[] gamePieces = new AbstractGamePiece[3];
        gamePieces[0] = new Deputy();
        gamePieces[1] = new Henchman();
        gamePieces[2] = new Kingpin();

        for (int i = 0; i < gamePieces.length; i++)
        {
            RowConstraints row = new RowConstraints();
            row.setPercentHeight(100 / (double) gamePieces.length);
            root.getRowConstraints().add(row);

            AbstractGamePiece piece = gamePieces[i];

            piece.setPosition(i, i + 1);

            root.add(new Label(piece.toString()), 0, i);
            root.add(new Label("Col: " + piece.getCol() + ", Row: " + piece.getRow() + ", Abbrev: " + piece.getAbbreviation()), 1, i);
            root.add(new Label("CanMove: " + piece.canMoveToLocation(null)), 2, i);
            root.add(new Label("isCaptured: " + piece.isCaptured(null)), 3, i);
            root.add(new Label("hasEscaped: " + piece.hasEscaped()), 4, i);
        }

        primaryStage.show();
    }
}

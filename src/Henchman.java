public class Henchman extends AbstractGamePiece
{
    public Henchman()
    {
        super("Henchman", "H", PLAYER_OUTLAWS);
    }
    public boolean hasEscaped()
    {
        return false;
    }
}
